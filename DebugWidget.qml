import QtQuick 2.0

/**
 * Widget that shows the size of its parent.
 *
 * Set its visible property to false in order to hide it.
 */
Rectangle {
    id: root

    property string widgetName: "DebugWidget"
    property color backgroundColor: "#66ff00"
    property color rectangleColor: "#0066ff"
    property color textColor: "#ff00ff"

    visible: true // Set to false in order to hide it and avoid messages to be printed
    enabled: false
    x: parent.x
    y: parent.y
    width: parent.width
    height: parent.height
    color: backgroundColor
    opacity: 0.5

    Text {
        id: aText
        anchors.centerIn: parent
        text: parent.width + 'x' + parent.height
        visible: root.visible

        Rectangle{
            anchors.fill: parent
            visible: root.visible
            color: rectangleColor

            Text {
                anchors.fill: parent
                visible: root.visible
                color: textColor
                text: aText.text
            }
        }
    }

    onWidthChanged: {
        if (root.visible) {
            console.log("Width of " + widgetName + ": " + width);
        }
    }

    onHeightChanged: {
        if (root.visible) {
            console.log("Height of " + widgetName + ": " + height);
        }
    }
}
