import QtQuick 2.9
import QtQuick.Controls 2.2
import QtQuick.Window 2.3
import Qt.labs.settings 1.0
import QtQuick.Layouts 1.3

/**
 * Prototype of an ListView with some items - and we can create/update/delete them - with state saving.
 *
 * For the layouts, read:
 * - http://doc.qt.io/qt-5/qml-qtquick-layouts-layout.html
 * - http://doc.qt.io/qt-5/qtquicklayouts-overview.html
 * - http://doc.qt.io/qt-5/qtquicklayouts-index.html
 */
ApplicationWindow {
    id: main

    /**
     * The JSON string that is saved to the settings.
     */
    property string datastore: ""
    property bool showDebugWidgets: false // Set to true in order to see the DebugWidget.

    /**
     * Loads the model from the settings.
     */
    function loadModel() {
        if (datastore !== "") {
            dataModel.clear();
            console.log("Loading " + datastore);
            var data = JSON.parse(datastore);
            for (var i = 0; i < data.length; ++i) {
                dataModel.append(data[i]);
            }
        }
    }

    /**
     * Saves the model to the settings.
     */
    function saveModel() {
        var data = [];
        for (var i = 0; i < dataModel.count; ++i) {
            data.push(dataModel.get(i));
        }
        datastore = JSON.stringify(data);
    }

    /**
     * Adds a new item to the model.
     * Its value is random.
     */
    function addNewItem() {
        var num = Math.round(Math.random() * 10);
        dataModel.append({
            "exampleString": "test" + num,
            "exampleInt": num
        });
        console.log("Add new item " + num);
    }

    /**
     * Removes one item from the model.
     */
    function removeOneItem() {
        if (dataModel.count > 0) {
            dataModel.remove(0, 1);
        }
        console.log("Remove one item");
    }

    /**
     * Changes one item with some random data.
     */
    function changeOneItem() {
        if (dataModel.count > 0) {
            var index = Math.floor(Math.random() * dataModel.count);
            var num = Math.round(Math.random() * 10);
            dataModel.setProperty(index, "exampleInt", num);
            dataModel.setProperty(index, "exampleString", "test" + num);
        }
    }

    width: 640
    height: 480
    visible: true

    /**
     * At startup, load the model from the settings.
     */
    Component.onCompleted: {
        loadModel();
    }

    /**
     * Before closing the app properly, save the model to the settings.
     */
    onClosing: {
        saveModel();
    }

    /**
     * We periodically save the model, just it case we close the app with an error.
     */
    Timer {
        running: true
        triggeredOnStart: false
        repeat: true
        interval: 1000 * 60 // each minute
        onTriggered: {
            saveModel();
        }
    }

    /**
     * Settings for the state saving of the model as a JSON string.
     */
    Settings {
        property alias datastore: main.datastore
    }

    /**
     * Our layout contains the buttons to edit the list items and the list items themselves.
     */
    ColumnLayout {
        anchors.fill: parent
        // Within a Layout widget, we use the Layout.* properties to manage the positionning of our widgets.
        Layout.leftMargin: 10

        /**
         * Buttons to create/update/delete the items.
         */
        RowLayout {
            Layout.fillHeight: false
            Layout.fillWidth: true

            Button {
                Layout.fillWidth: true
                text: qsTr("Add one")
                onClicked: {
                    addNewItem();
                }
            }
            Button {
                Layout.fillWidth: true
                text: qsTr("Remove one")
                onClicked: {
                    removeOneItem();
                }
            }
            Button {
                Layout.fillWidth: true
                text: qsTr("Change one")
                onClicked: {
                    changeOneItem();
                }
            }

            DebugWidget {
                visible: showDebugWidgets
            }
        }

        /**
         * Shows the list items.
         */
        ScrollView {
            Layout.fillHeight: true
            Layout.fillWidth: true
            // FIXME: The scrollbar is visible, but it should be interactive, too.
            ScrollBar.vertical.interactive: true
            ScrollBar.vertical.policy: ScrollBar.AlwaysOn
            clip: true

            contentItem: ListView {
                //height: childrenRect.height
                //width: childrenRect.width

                model: ListModel {
                    id: dataModel

                    ListElement {
                        exampleString: "test1"
                        exampleInt: 1
                    }
                }
                delegate: ItemWidget {
                    someString: exampleString + " " + exampleInt
                }
            }

            DebugWidget {
                visible: showDebugWidgets
            }
        }
    }
}
